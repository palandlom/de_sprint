import os
import helpers as hp
import requests as req
from bs4 import BeautifulSoup
import json

out_dir = "./data"

req_headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'}

vacations = []
page_num = 0
lastpage_number = -1
while True:
    # Get search page content ...
    req_str = f"https://hh.ru/search/vacancy?text=python&salary=&clusters=true&ored_clusters=true&enable_snippets=true&page={page_num}&hhtmFrom=vacancy_search_list"
    resp = req.get(req_str, headers=req_headers)
    soup = BeautifulSoup(resp.text, "lxml")

    # ... collect vacations from it
    vcs = hp.get_vacations_from_page(soup, req_headers)
    vacations.extend(vcs)

    if lastpage_number < 0:
        lastpage_number = hp.get_lastpage_number(soup)

    if page_num == lastpage_number:
        break

    if page_num == 2:
        break

    page_num += 1

out_file_path = os.path.join(out_dir, "vacations.json")
with open(out_file_path, 'w') as f:
    json.dump({"data": vacations}, f,  indent=4, ensure_ascii=False)

print(f"Got {len(vacations)} vacations from {page_num + 1} pages - saved in {out_file_path}")
