import string

import bs4
import requests as req
from bs4 import BeautifulSoup


def get_lastpage_number(filled_bs_parser: bs4.BeautifulSoup) -> int:
    # block with pages - e.g. 0,1 .. 40
    pager_tag = filled_bs_parser.find(class_="pager")

    last_page_num = 0
    pager_span_tags = pager_tag.find_all("span", recursive=False)
    for pager_span_tag in pager_span_tags:
        page_tag = pager_span_tag.find(attrs={"data-qa": "pager-page"})
        page_num = int(page_tag.find("span").text)
        last_page_num = page_num if last_page_num < page_num else last_page_num

    return last_page_num


def get_vacations_from_page(filled_bs_parser: bs4.BeautifulSoup, req_headers: dict) -> list:
    vacations = []

    # Get all vacation tags...
    for vac_tag in filled_bs_parser.find_all(class_="vacancy-serp-item-body__main-info"):
        # ... find name and region in each of them
        vacname_tag = vac_tag.find(class_="serp-item__title")
        region_tag = vac_tag.find(attrs={"data-qa": "vacancy-serp__vacancy-address"})

        # Go to the vacation page ...
        vac_url = vacname_tag.attrs["href"]
        vac_resp = req.get(vac_url, headers=req_headers)
        vac_soup = BeautifulSoup(vac_resp.text, "lxml")

        # ... get salary and exp from it
        vac_salary = vac_soup.find(attrs={"data-qa": "vacancy-salary"}).text
        vac_exp = vac_soup.find(attrs={"data-qa": "vacancy-experience"}).text

        vacation = {
            "title": vacname_tag.text,
            "work experience": vac_exp,
            "salary": vac_salary,
            "region": region_tag.text}

        vacations.append(vacation)
    return vacations


