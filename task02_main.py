"""
2. Палиндром строки
"""

inp = input()
# inp="taco cat"
# inp="rotator"
# inp="равен"

s = inp.lower()
s = s.replace(" ", "")
rev_s = ""

for i in range(len(s), 0, -1):
    rev_s += s[i-1]

print(s == rev_s)
