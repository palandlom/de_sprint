import math

"""
3. из арабских в римские
Ввод: x = 3
Вывод: “III”

Пример 2:
Ввод: x = 9

Вывод: “IX”
Пример 3:

Ввод: x = 1945
Вывод: “MCMXLV”

Гарантируется, что введенное число X будет находиться в диапазоне от 1 до 2000 
 
"""

num = input()
num = int(num)


rnum_section = {
    # 0,1,2,3,4,5,6,7,8,9 = qty 10
    0: ['', "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"],

    # 10, 20, 30, 40, 50, 60 70 80 90
    1: ['', "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"],

    # 100 200 ... 900
    2: ['', "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"],

    # 1000, 2000
    3: ['', "M", "MM"],
}

part1000 = rnum_section[3][math.floor(num / 1000 % 10)]
part100 = rnum_section[2][math.floor(num / 100 % 10)]
part10 = rnum_section[1][math.floor(num / 10 % 10)]
part1 = rnum_section[0][math.floor(num % 10)]

roman = f"{part1000}{part100}{part10}{part1}"
print(roman)
