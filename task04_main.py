import math

"""
 Дана строка X, состоящая только из символов “{“, “}”, “[“, “]”, “(“, “)”. 
 Программа должна вывести True, в том случае если все открытые скобки закрыты. Например: “[()]{}”, 
 все открытые скобки закрыты закрывающимися скобками, потому вывод будет True. 
 В случае же, если строка будет похожа на: “{{{}”, то вывод будет False, т.к. не все открытые скобки закрыты.

Пример 1:
Ввод: x = “[{}({})]”
Вывод: True

Пример 2:
Ввод: x = “{]”
Вывод: False

Пример 3:
Ввод: x = “{“
Вывод: False

Гарантируется, что введенная строка X будет содержать только скобки и не будет пустой. 
"""

strn = input()
# strn = "[{}({})]"
# strn = "{]"
# strn = "{"

q_count = 0  # []
r_count = 0  # ()
c_count = 0  # {}

for s in strn:
    if s == "[":
        q_count += 1
        continue

    if s == "]":
        q_count -= 1
        continue

    if s == "(":
        r_count += 1
        continue

    if s == ")":
        r_count -= 1
        continue

    if s == "{":
        c_count += 1
        continue

    if s == "}":
        c_count -= 1
        continue

    raise ValueError(f"wrong symbol {s}")

print(c_count == 0 and r_count == 0 and q_count == 0)

